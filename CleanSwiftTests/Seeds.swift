@testable import CleanSwift
@testable import ObjectMapper

import XCTest

struct Seeds {
    
    private static let couponDict: [String: Any] = ["Buttons": ["action": 1, "text":"Ver online"], "description":"Los mejores descuentos en The Body Shop"]
    public static let store1Dict: [String: Any] = ["distance":"0,4063797", "latitude":"41,38345", "longitude":"2,17269", "phone_number":"933437097", "store_address":"Carrer de la Portaferrisa, 11", "store_city":"Barcelona", "store_id":"26342", "store_name":"The Body Shop", "store_zip_code":"08002", "web_url":"http://www.thebodyshop.es"]
    private static let offer1Dic: [String: Any] = ["catalog_id": "402150", "date_end": "1480460400", "retailer_id": "413", "retailer_name":"The Body Shop"]
    private static let offer2Dic: [String: Any] = ["catalog_id": "402150", "date_end": "1480460400", "retailer_id": "413", "retailer_name":"The Body Shop 2", "nearest_store": store1Dict, "coupon": couponDict]
    private static let offer3Dic: [String: Any] = ["catalog_id": "402150", "date_end": "1480460400", "retailer_id": "413", "retailer_name":"The Body Shop", "coupon": Seeds.couponDict]
    
    struct Offers {
        
        static let offer1 = Mapper<Offer>().map(JSON: Seeds.offer1Dic)
        static let offer2 = Mapper<Offer>().map(JSON: Seeds.offer2Dic)
        static let offer3 = Mapper<Offer>().map(JSON: Seeds.offer3Dic)
        static let store1 = Mapper<Store>().map(JSON: Seeds.store1Dict)
        
        public static func getOfferItems() -> [OfferItem] {
            let coupons: [Offer] = [Seeds.Offers.offer2!, Seeds.Offers.offer3!]
            let catalogs: [Offer] = [Seeds.Offers.offer1!]
            let catalogItems = OfferItem(sectionName: "Catalogs", presentableItems: catalogs)
            let couponItems = OfferItem(sectionName: "Coupons", presentableItems: coupons)
            
            return [catalogItems, couponItems]
        }
    }
}
