//
//  OffersAPI.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/22/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

@testable import CleanSwift
import XCTest

class OffersAPITests: XCTestCase {
    // MARK: - Subject under test
    
    var sut: OffersAPI!
    var testOffers: [Offer]!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupOffersAPI()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup -
    func setupOffersAPI() {
        sut = OffersAPI()
        
        OffersWorkerTests.testOffers = [Seeds.Offers.offer1!, Seeds.Offers.offer2!, Seeds.Offers.offer3!]
    }
    
    // MARK: - Tests - 
    func testFetchOffersShouldReturnListOfOffers() {
        // Given
        
        // When
        var fetchedOffers: [Offer]?
        let expect = expectation(description: "Wait for fetchOffers() to return")
        
        sut.fetchOffers { (offers: () throws -> [Offer]?) -> Void in
            do {
                fetchedOffers = try offers()
            } catch {}
            
            expect.fulfill()
        }
        waitForExpectations(timeout: 3.0)
        
        // Then
        XCTAssertNotNil(fetchedOffers)
    }
}
