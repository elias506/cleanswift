@testable import CleanSwift
import XCTest

class OffersWorkerTests: XCTestCase
{
    // MARK: - Subject under test -
    var sut: OffersWorker!
    static var testOffers = [Offer]()
    
    // MARK: - Test lifecycle -
    override func setUp() {
        super.setUp()
        setupOffersWorker()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup -
    func setupOffersWorker() {
        sut = OffersWorker(offersStore: OffersAPISpy())
        
        OffersWorkerTests.testOffers = [Seeds.Offers.offer1, Seeds.Offers.offer2, Seeds.Offers.offer3] as! [Offer]
    }
    
    // MARK: - OffersAPISpy -
    class OffersAPISpy: OffersAPI {
        // MARK: Method call expectations
        var fetchOffersCalled = false
        
        // MARK: Spied methods
        override func fetchOffers(completionHandler: @escaping (() throws -> [Offer]?) -> Void) {
            fetchOffersCalled = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                completionHandler { () -> [Offer] in
                    return OffersWorkerTests.testOffers
                }
            }
        }
    }
    
    // MARK: - Tests - 
    func testFetchOffersShouldReturnListOfOffers() {
        // Given
        let offersAPISpy = sut.offersStore as! OffersAPISpy
        
        // When
        var fetchedOffers = [Offer]()
        let expect = expectation(description: "Wait for fetchOffers() to return")
        sut.fetchOffers { (offers) in
            if let offers = offers {
                fetchedOffers = offers
                expect.fulfill()
            }
        }
        waitForExpectations(timeout: 1.1)
        
        // Then
        XCTAssert(offersAPISpy.fetchOffersCalled, "Calling fetchOffers() should ask the data store for a list of offers")
        XCTAssertEqual(fetchedOffers.count, OffersWorkerTests.testOffers.count, "fetchOffers() should return a list of offers")
    }
}
