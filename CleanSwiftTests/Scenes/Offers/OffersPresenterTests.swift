//
//  OffersPresenterTests.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/22/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

@testable import CleanSwift
import XCTest

class OffersPresenterTests: XCTestCase {
    
    // MARK: - Subject under test -
    var sut: OffersPresenter!
    
    // MARK: - Test lifecycle -
    override func setUp() {
        super.setUp()
        setupOffersPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup -
    func setupOffersPresenter() {
        sut = OffersPresenter()
    }
    

    class OffersDisplayLogicSpy: OffersDisplayLogic {
        
        // MARK: Method call expectations
        var displayFetchedOffersCalled = false
        
        // MARK: Argument expectations
         var viewModel: Offers.FetchOffers.ViewModel!
        
        // MARK: Spied methods
        func displayOffers(viewModel: Offers.FetchOffers.ViewModel) {
            displayFetchedOffersCalled = true
            self.viewModel = viewModel
        }
    }
    
    // MARK: - Tests -
    func testPresentFetchedOffersShouldFormatFetchedOffersForDisplay() {
        // Given
        let offersDisplayLogicSpy = OffersDisplayLogicSpy()
        sut.viewController = offersDisplayLogicSpy
        
        // When
        let response = Offers.FetchOffers.Response(offers: Seeds.Offers.getOfferItems())
        sut.presentOffers(response: response)
        
        // Then
        let offers = offersDisplayLogicSpy.viewModel.offers
        XCTAssertNotNil(offers)
        
        // Catalogs
        XCTAssertEqual(offers[0].sectionName, "Catalogs")
        XCTAssertEqual(offers[0].presentableItem(at: 0)?.storeNameValue, "The Body Shop")
        
        // Coupons
        XCTAssertEqual(offers[1].sectionName, "Coupons")
        if let storePresentable = offers[1].presentableItem(at: 1)?.storePresentable {
            var messageNotFound: String = ""
            
            switch storePresentable.viewState {
            case .hasNearStore:
                break
            case .nearStoreNotFound(let message):
                messageNotFound = message
            }
            XCTAssertEqual(messageNotFound, "No hemos encontrado tiendas cercanas")
        } else {
             XCTFail("Expected to have a store")
        }
    }
    
    func testPresentOffersShouldAskViewControllerToDisplayOffers() {
        // Given
        let offersDisplayLogicSpy = OffersDisplayLogicSpy()
        sut.viewController = offersDisplayLogicSpy

        // When
        let response = Offers.FetchOffers.Response(offers: Seeds.Offers.getOfferItems())
        sut.presentOffers(response: response)

        // Then
        XCTAssert(offersDisplayLogicSpy.displayFetchedOffersCalled, "Presenting offers should ask view controller to display them")
    }
}
