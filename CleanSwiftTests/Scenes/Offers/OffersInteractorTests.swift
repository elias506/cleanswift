//
//  OffersInteractorTests.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/22/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

@testable import CleanSwift
import XCTest

class OffersInteractorTests: XCTestCase {
    
    // MARK: - Subject under test -
    var sut: OffersInteractor!
    
    // MARK: - Test lifecycle -
    override func setUp() {
        super.setUp()
        setupOffersInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup -
    func setupOffersInteractor() {
        sut = OffersInteractor()
    }

    class OffersPresentationLogicSpy: OffersPresentationLogic {
        
        // MARK: Method call expectations
        var presentFetchedOffersCalled = false
        
        // MARK: Spied methods
        func presentOffers(response: Offers.FetchOffers.Response) {
            presentFetchedOffersCalled = true
        }
    }
    
    class OffersWorkerSpy: OffersWorker {
        
        // MARK: Method call expectations
        var fetchOffersCalled = false
        
        // MARK: Spied methods
        override func fetchOffers(completionHandler: @escaping ([Offer]?) -> Void) {
            fetchOffersCalled = true
            completionHandler([Seeds.Offers.offer1!, Seeds.Offers.offer2!, Seeds.Offers.offer3!])
        }
    }
    
    // MARK: - Tests -
    func testFetchOffersShouldAskOffersWorkerToFetchOffersAndPresenterResult() {
        // Given
        let offersPresentationLogicSpy = OffersPresentationLogicSpy()
        sut.presenter = offersPresentationLogicSpy
        let offersWorkerSpy = OffersWorkerSpy(offersStore: OffersAPI())
        sut.offersWorker = offersWorkerSpy
        
        // When
        let request = Offers.FetchOffers.Request()
        sut.fetchOffers(request: request)
        
        // Then
        XCTAssert(offersWorkerSpy.fetchOffersCalled, "FetchOffers() should ask OffersWorker to fetch Offers")
        XCTAssert(offersPresentationLogicSpy.presentFetchedOffersCalled, "FetchOffers() should ask presenter to format Offers result")
    }
}
