//
//  OffersViewControllerTests.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/22/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

@testable import CleanSwift
import XCTest
import UIKit

class OffersViewControllerTests: XCTestCase {
    
    // MARK: - Subject under test -
    var sut: OffersViewController!
    var window: UIWindow!
    
    let layout = UICollectionViewFlowLayout()
    
    // MARK: - Test lifecycle -
    override func setUp() {
        super.setUp()
        window = UIWindow()
        
        layout.itemSize = CGSize(width: 180, height: 120)
        
        setupOffersViewController()
    }
    
    override func tearDown() {
        window = nil
        super.tearDown()
    }
    
    // MARK: - Test setup -
    func setupOffersViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    class OffersBusinessLogicSpy: OffersBusinessLogic, OffersDataStore {
        
        var offerItems: [OfferItem]?
        
        // MARK: Method call expectations
        var displayOffersCalled = false
        
        // MARK: Spied methods
        func fetchOffers(request: Offers.FetchOffers.Request) {
            displayOffersCalled = true
        }
    }
    
    class CollectionViewSpy: UICollectionView {
        // MARK: Method call expectations
        var reloadDataCalled = false
        
        // MARK: Spied methods
        override func reloadData() {
            reloadDataCalled = true
        }
    }
    
    // MARK: - Tests
    
    func testShouldFetchOffersWhenViewDidAppear() {
        // Given
        let offersBusinessLogicSpy = OffersBusinessLogicSpy()
        sut.interactor = offersBusinessLogicSpy
        loadView()
        
        // When
        sut.viewDidAppear(true)
        
        // Then
        XCTAssert(offersBusinessLogicSpy.displayOffersCalled, "Should fetch offers right after the view loads")
    }
    
    func testShouldDisplayOffers() {
        // Given
        let collectionViewSpy = CollectionViewSpy(frame: CGRect(x: 0, y: 0, width: 600, height: 400), collectionViewLayout: layout)
        sut.collectionView = collectionViewSpy

        // When
        let viewModel = Offers.FetchOffers.ViewModel(offers: Seeds.Offers.getOfferItems())
        sut.displayOffers(viewModel: viewModel)

        // Then
        XCTAssert(collectionViewSpy.reloadDataCalled, "Displaying fetched offers should reload the table view")
    }
    
    func testNumberOfSectionsAndItemsInCollectionViewShouldBeTwo() {
        // Given
        let collectionViewSpy = CollectionViewSpy(frame: CGRect(x: 0, y: 0, width: 600, height: 400), collectionViewLayout: layout)
        sut.collectionView = collectionViewSpy
        
        // When
        let viewModel = Offers.FetchOffers.ViewModel(offers: Seeds.Offers.getOfferItems())
        sut.displayOffers(viewModel: viewModel)
        
        let numberOfSections = sut.collectionView.numberOfSections
        let numberOfItemsInSection0 = sut.collectionView.numberOfItems(inSection: 0)
        let numberOfItemsInSection1 = sut.collectionView.numberOfItems(inSection: 1)
        
        // Then
        XCTAssertEqual(numberOfSections, 2, "The number of collection view sections should be 2")
        XCTAssertEqual(numberOfItemsInSection0, 1, "The number of items in section 0 should be 1")
        XCTAssertEqual(numberOfItemsInSection1, 2, "The number of items in section 1 should be 2")
    }
}
