//
//  NearestStorePresenter.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/22/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//
@testable import CleanSwift
import XCTest

class NearestStorePresenterTests: XCTestCase {
    
    // MARK: - Subject under test -
    var sut: NearestStorePresenter!
    
    // MARK: - Test lifecycle -
    override func setUp() {
        super.setUp()
        setupShowStorePresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup -
    func setupShowStorePresenter() {
        sut = NearestStorePresenter()
    }
    
    class NearestStoreDisplayLogicSpy: NearestStoreDisplayLogic {
        // MARK: Method call expectations
        
        var displayStoreCalled = false
        
        // MARK: Argument expectations
        
        var displayStoreViewModel: NearestStore.Store.ViewModel!
        
        // MARK: Spied methods
        
        func displayStoreView(viewModel: NearestStore.Store.ViewModel) {
            displayStoreCalled = true
            displayStoreViewModel = viewModel
        }
    }
    
    // MARK: - Tests -
    func testPresentStoreShouldAskViewControllerToDisplayStore() {
        // Given
        let nearestStoreDisplayLogicSpy = NearestStoreDisplayLogicSpy()
        sut.viewController = nearestStoreDisplayLogicSpy
        
        // When
        let store = Seeds.Offers.store1
        let response = NearestStore.Store.Response(presentableItem: store, view: Bundle.loadView(type: StoreFoundView.self))
        sut.presentStoreView(response: response)
        
        // Then
        XCTAssert(nearestStoreDisplayLogicSpy.displayStoreCalled, "Presenting store should ask view controller to display it")
    }
}
