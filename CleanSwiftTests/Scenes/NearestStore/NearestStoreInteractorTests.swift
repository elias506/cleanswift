//
//  NearestStoreInteractorTests.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/22/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

@testable import CleanSwift
import XCTest

class NearestStoreInteractorTests: XCTestCase {
    
    // MARK: - Subject under test -
    var sut: NearestStoreInteractor!
    
    // MARK: - Test lifecycle -
    override func setUp() {
        super.setUp()
        setupShowStoreInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup -
    func setupShowStoreInteractor() {
        sut = NearestStoreInteractor()
    }
    
    // MARK: - Tests - 
    class NearestStorePresentationLogicSpy : NearestStorePresentationLogic {
        
        // MARK: Method call expectations

        var presentStoreViewCalled = false
        
        // MARK: Spied methods

        func presentStoreView(response: NearestStore.Store.Response) {
            presentStoreViewCalled = true
        }
    }
    
    func testGetStoreShouldPresentStoreView() {
        // Given
        let nearestStorePresentationLogicSpy = NearestStorePresentationLogicSpy()
        sut.presenter = nearestStorePresentationLogicSpy
        sut.presentableItem = Seeds.Offers.store1
        
        // When
        let request = NearestStore.Store.Request()
        sut.getNearestStoreView(request: request)
        
        // Then
        XCTAssert(nearestStorePresentationLogicSpy.presentStoreViewCalled, "PresentStoreView() should ask presenter to format the Store")
    }
}
