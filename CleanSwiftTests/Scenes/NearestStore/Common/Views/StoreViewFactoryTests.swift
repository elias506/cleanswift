//
//  StoreViewFactoryTests.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/21/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import XCTest
@testable import CleanSwift

class StoreViewFactoryTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetNearestStoreViewForStoreFound() {
        let view = StoreViewFactory.getNearestStoreView(for: .hasNearStore)
        XCTAssertTrue(view as? StoreFoundView != nil)
    }
    
    func testGetNearestStoreViewForStoreNotFound() {
        let view = StoreViewFactory.getNearestStoreView(for: .nearStoreNotFound(message: ""))
        XCTAssertTrue(view as? StoreNotFoundView != nil)
    }
}
