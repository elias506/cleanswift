//
//  NearestStoreViewControllerTests.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/22/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

@testable import CleanSwift
import XCTest

class NearestStoreViewControllerTests: XCTestCase {
    
    // MARK: - Subject under test -
    var sut: NearestStoreViewController!
    var window: UIWindow!
    
    // MARK: - Test lifecycle -
    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupNearestStoreViewController()
    }
    
    override func tearDown() {
        window = nil
        super.tearDown()
    }
    
    // MARK: - Test setup -
    func setupNearestStoreViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "NearestStoreViewController") as! NearestStoreViewController
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    class NearestStoreBusinessLogicSpy: NearestStoreBusinessLogic, NearestStoreDataStore {
        var presentableItem: StoreViewPresentable?
        
        // MARK: Method call expectations
        
        var getNearestStoreViewCalled = false
        
        // MARK: Spied methods
        
        func getNearestStoreView(request: NearestStore.Store.Request) {
            getNearestStoreViewCalled = true
        }
    }
    
    // MARK: - Tests -
    func testShouldShowStoreWhenViewIsLoaded() {
        // Given
        let nearestStoreBusinessLogicSpy = NearestStoreBusinessLogicSpy()
        nearestStoreBusinessLogicSpy.presentableItem = Seeds.Offers.store1
        sut.interactor = nearestStoreBusinessLogicSpy
        
        // When
        loadView()
        
        // Then
        XCTAssert(nearestStoreBusinessLogicSpy.getNearestStoreViewCalled, "Should show store when the view is loaded")
    }
    
    func testDisplayStoreViewShouldDisplayStoreFoundView() {
        // Given
        let nearestStoreBusinessLogicSpy = NearestStoreBusinessLogicSpy()
        sut.interactor = nearestStoreBusinessLogicSpy
        loadView()
        
        // When
        let presentableItem = Seeds.Offers.store1!
        let viewModel = NearestStore.Store.ViewModel(presentableItem: presentableItem, view: Bundle.loadView(type: StoreFoundView.self))
        sut.displayStoreView(viewModel: viewModel)
        
        // Then
        var hasStoreFoundView = false
        sut.view.subviews.forEach { (view) in
            if ((view as? StoreFoundView) != nil) {
                hasStoreFoundView = true
            }
        }
        
        XCTAssertTrue(hasStoreFoundView)
    }
    
    func testDisplayStoreViewShouldDisplayStoreNotFoundView() {
        // Given
        let nearestStoreBusinessLogicSpy = NearestStoreBusinessLogicSpy()
        sut.interactor = nearestStoreBusinessLogicSpy
        loadView()
        
        // When
        let presentableItem = Seeds.Offers.store1!
        let viewModel = NearestStore.Store.ViewModel(presentableItem: presentableItem, view: Bundle.loadView(type: StoreNotFoundView.self))
        sut.displayStoreView(viewModel: viewModel)
        
        // Then
        let storeNotFoundView = sut.view.subviews.filter {($0 as? StoreNotFoundView) != nil}.first
        XCTAssertNotNil(storeNotFoundView)
    }
}
