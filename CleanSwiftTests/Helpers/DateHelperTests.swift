//
//  DateHelperTests.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/21/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import XCTest
@testable import CleanSwift

class DateHelperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIntervalIsConvertedToCorrectFormat() {
        let formattedDate = DateHelper.convertIntervalToStringDate(interval: 1540148843, using: "dd/MM/yyyy")
        XCTAssertEqual(formattedDate, "21/10/2018")
    }
}
