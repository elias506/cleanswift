//
//  ItemSizerTests.swift
//  CleanSwiftTests
//
//  Created by Elias Esquivel on 10/21/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import XCTest
@testable import CleanSwift

class ItemSizerTests: XCTestCase {
    var containerView = UIView()
    let ratio = CGFloat(2)
    override func setUp() {
        super.setUp()
        containerView.frame = CGRect(x: 0, y: 0, width: 400, height: 600)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testItemWidth() {
        let itemSizer = ItemSizer(view: containerView, spacing: 8, ratio: ratio, itemsPerPage: 2)
        XCTAssertEqual(itemSizer.width, CGFloat(188.0))
    }
    
    func testItemHeight() {
        let itemSizer = ItemSizer(view: containerView, spacing: 8, ratio: ratio, itemsPerPage: 2)
        XCTAssertEqual(itemSizer.height, CGFloat(376.0))
    }
}
