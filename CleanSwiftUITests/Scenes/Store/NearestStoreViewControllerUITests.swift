//
//  NearestStoreViewControllerUITests.swift
//  CleanSwiftUITests
//
//  Created by Elias Esquivel on 10/22/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import XCTest

class NearestStoreViewControllerUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testOpenACatalogWithAStoreShouldPresentTheStoreInformation() {
        let app = XCUIApplication()
        app.collectionViews.cells.otherElements.containing(.staticText, identifier:"IKEA").children(matching: .other).element(boundBy: 0).tap()
        XCTAssertTrue(app.staticTexts["HEMOS ENCONTRADO UNA TIENDA CERCANA"].exists)
        XCTAssertTrue(app.staticTexts["IKEA Gran Vía"].exists)
        XCTAssertTrue(app.staticTexts["Dirección: Districte Econòmic, L'Hospitalet de Llobregat"].exists)
        XCTAssertTrue(app.staticTexts["Sitio web: http://www.ikea.com/es/es/preindex.html"].exists)
        
    }
    
    func testOpenACouponWithAStoreShouldPresentTheStoreNotFoundView() {
        let app = XCUIApplication()
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery.children(matching: .cell).element(boundBy: 1).otherElements.containing(.image, identifier:"favorites").element.swipeUp()
        collectionViewsQuery.cells.otherElements.containing(.staticText, identifier:"NESCAFÉ® Dolce Gusto®").children(matching: .other).element(boundBy: 0).tap()
        
        XCTAssertTrue(app.staticTexts["No hemos encontrado tiendas cercanas"].exists)
    }
    
    func testScreenHasTitle() {
        let app = XCUIApplication()
        app.collectionViews.children(matching: .cell).element(boundBy: 0).otherElements.containing(.image, identifier:"favorites").children(matching: .other).element(boundBy: 0).tap()
        XCTAssertTrue(app.navigationBars["Tienda"].otherElements["Tienda"].exists)
    }
}
