//
//  OfferCellUITests.swift
//  CleanSwiftUITests
//
//  Created by Elias Esquivel on 10/23/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import XCTest

class OfferCellUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCellHasRetailerDateImageAndButtonsExists() {
        
        let app = XCUIApplication()
        let cell = app.collectionViews.children(matching: .cell).element(boundBy: 0)
        let favoritesElement = cell.otherElements.containing(.image, identifier:"favorites").element
        
        XCTAssertTrue(cell.staticTexts["MANGO"].exists)
        XCTAssertTrue(cell.staticTexts["03/12/2016"].exists)
        XCTAssertTrue(cell.buttons["share"].exists)
        XCTAssertTrue(cell.buttons["save"].exists)
        XCTAssertTrue(favoritesElement.exists)
    }
}
