//
//  OffersViewControllerUITests.swift
//  CleanSwiftUITests
//
//  Created by Elias Esquivel on 10/22/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import XCTest

class OffersViewControllerUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testOpenOffersAndDisplaysViewTitle() {
        let app = XCUIApplication()
        XCTAssertTrue(app.navigationBars["Tiendeo"].otherElements["Tiendeo"].exists)
    }
    
    func testOpenOffersAndDisplaysHeaderTitles() {
        
        let app = XCUIApplication()
        let collectionViewsQuery = app.collectionViews
        XCTAssertTrue(collectionViewsQuery.staticTexts["CATÁLOGOS"].exists)
        collectionViewsQuery.children(matching: .cell).element(boundBy: 0).otherElements.containing(.image, identifier:"favorites").element.swipeUp()
        XCTAssertTrue(collectionViewsQuery.staticTexts["CUPONES"].exists)
    }
}
