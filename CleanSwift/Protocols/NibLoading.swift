//
//  NibLoading.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/19/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

public protocol NibLoading {
    static func loadNib() -> UINib
}

public extension NibLoading where Self: AnyObject {
    
    static func loadNib() -> UINib {
        let nibName = String(describing: self)
        return UINib(nibName: nibName, bundle: Bundle(for: self as AnyClass))
    }
}
