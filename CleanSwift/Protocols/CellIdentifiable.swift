//
//  CellIdentifiable.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/19/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import Foundation

public protocol CellIdentifiable {
    static var cellIdentifier: String { get }
}

public extension CellIdentifiable {
    
    static var cellIdentifier: String {
        return String(describing: self)
    }
}
