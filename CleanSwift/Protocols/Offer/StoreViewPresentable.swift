//
//  StoreViewPresentable.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/20/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

// Represents the contract to display the data in the StoreView 
protocol StoreViewPresentable: OfferPresentable {
    var viewState: StoreViewState { get }
    var phoneNumberValue: String? { get }
    var addressValue: String? { get }
    var webSiteUrl: String? { get }
}
