//
//  OfferPresentable.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/21/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

protocol OfferPresentable {
    var storeNameValue: String? { get }
}
