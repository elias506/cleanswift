//
//  OfferCellPresentable.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

enum OfferCellType {
    case catalog
    case coupon
}
// Represents the contract to display data in the OfferCell
protocol OfferCellPresentable: OfferPresentable {
    var offerDate: String? { get }
    var offerImage: UIImage? { get }
    var offerType: OfferCellType { get }
    var couponText: String? { get }
    var storePresentable: StoreViewPresentable { get }
}
