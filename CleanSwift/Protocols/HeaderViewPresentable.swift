//
//  HeaderViewPresentable.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

// Represents the contract to display the needed data in the HeaderView
protocol HeaderViewPresentable {
    var title: String { get }
}
