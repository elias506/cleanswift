//
//  DateHelper.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/19/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import Foundation

struct DateHelper {
    
    /**
     Converts an interval into a string date
     - parameter interval: interval value.
     - parameter format: desired Date format.
     - returns: An optional string date
     */
    public static func convertIntervalToStringDate(interval: Double, using format: String) -> String? {
        let date = Date(timeIntervalSince1970: interval)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
}
