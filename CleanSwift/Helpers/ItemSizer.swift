//
//  ItemSizer.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/19/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import Foundation
import UIKit

/**
 Provides the corresponding item's width and height based on the available space and
 the spacing needed by keeping the ratio of the element.
 */
struct ItemSizer {
    var view: UIView
    var spacing: CGFloat
    var ratio: CGFloat
    var itemsPerPage: Int
    
    private var viewWidth: CGFloat {
        return view.frame.size.width
    }
    
    private var viewWidthWithoutMargins: CGFloat {
        let numberOfMargins = itemsPerPage + 1
        let margins = spacing * CGFloat(numberOfMargins)
        return viewWidth - margins
    }
    
    public var width: CGFloat {
        return viewWidthWithoutMargins / CGFloat(itemsPerPage)
    }
    
    public var height: CGFloat {
        return width * ratio
    }
}
