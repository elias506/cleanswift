//
//  Bundle+Loading.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/20/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

public extension Bundle {
    
    /**
     Load the view from Xib.
     - parameter name: nib name, if empty assume the view name and nib name are same.
     - parameter type: view type.
     */
    class func loadView<T>(fromNib name: String? = nil, type: T.Type, owner: AnyObject? = nil, fromCommons: Bool = false) -> T {
        var fileName = name
        if name == nil {
            fileName = String(describing: type)
        }
        
        let bundle = fromCommons ? Bundle(for: T.self as! AnyClass) : Bundle.main
        if let view = bundle.loadNibNamed(fileName!, owner: owner, options: nil)?.first as? T {
            return view
        }
        
        fatalError("Could not load view with type " + String(describing: type))
    }
}

