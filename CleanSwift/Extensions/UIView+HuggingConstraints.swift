//
//  UIView+HuggingConstraints.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/20/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//
import UIKit

public extension UIView {
    /**
     Pin view edges to superview edges by adding top, bottom, trailing and leading constraints.
     - parameter insets: Values to apply for appropriate constraints. UIEdgeInsets.zero is default.
     */
    @discardableResult public func activateSuperviewHuggingConstraints(insets: UIEdgeInsets = UIEdgeInsets.zero) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        let views = ["view": self]
        let metrics = ["top": insets.top, "left": insets.left, "bottom": insets.bottom, "right": insets.right]
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-left-[view]-right-|", options: [], metrics: metrics, views: views)
        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "V:|-top-[view]-bottom-|", options: [], metrics: metrics, views: views))
        NSLayoutConstraint.activate(constraints)
        return constraints
    }
}
