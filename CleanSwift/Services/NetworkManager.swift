//
//  NetworkManager.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import Alamofire
import ObjectMapper

class NetworkManager {
    static let shared = NetworkManager()
    
    /**
     Call to backend to retrieve the available offers.
     - parameter completion: a closure that contains the offers
     */
    public func getOffers(completion: @escaping ([Offer]?) -> Void) {
        self.request(for: "https://interview-ios.firebaseio.com/offers.json") { (response, error) in
            guard let response = response,
                error == nil else {
                    print("Error while fetching offers: \(String(describing: error))")
                    completion(nil)
                    return
            }
            
            let offers = Mapper<Offer>().mapArray(JSONObject: response)
            completion(offers)
        }
    }
    
    private func request(for url: String, completion: @escaping (_ response: Any?, _ error: Error?) -> Void) {
        Alamofire.request(url,
                          parameters: nil,
                          headers: nil)
            .responseJSON { response in
                guard response.result.isSuccess,
                    let value = response.result.value else {
                        completion(nil, response.result.error)
                        return
                }
                
                completion(value, nil)
        }
    }
}
