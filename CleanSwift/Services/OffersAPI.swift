//
//  OffersAPI.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/19/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import Foundation

public class OffersAPI: OffersStoreProtocol {
    func fetchOffers(completionHandler: @escaping (() throws -> [Offer]?) -> Void) {
        NetworkManager.shared.getOffers { (offers) in
            completionHandler { return offers }
        }
    }
}
