//
//  StorePresentView.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/20/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

class StoreFoundView: StoreView {
    @IBOutlet weak var retailerNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var siteUrlLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func configure(with presentableItem: StoreViewPresentable) {
        retailerNameLabel.text = presentableItem.storeNameValue
        addressLabel.text = presentableItem.addressValue
        phoneNumberLabel.text = presentableItem.phoneNumberValue
        siteUrlLabel.text = presentableItem.webSiteUrl
    }
}
