//
//  StoreViewFactory.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/20/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

enum StoreViewState {
    case hasNearStore
    case nearStoreNotFound(message: String)
}

struct StoreViewFactory {
    public static func getNearestStoreView(for state: StoreViewState) -> StoreView {
        switch state {
        case .hasNearStore:
            return Bundle.loadView(type: StoreFoundView.self)
        case .nearStoreNotFound:
            return Bundle.loadView(type: StoreNotFoundView.self)
        }
    }
}
