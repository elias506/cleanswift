//
//  NoStorePresentView.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/20/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

class StoreNotFoundView: StoreView {
    @IBOutlet weak var storeNotFoundLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func configure(with presentableItem: StoreViewPresentable) {
        switch presentableItem.viewState {
        case .hasNearStore:
            break
        case .nearStoreNotFound(let message):
            storeNotFoundLabel.text = message
        }
    }
}
