//
//  StoreView.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/20/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

class StoreView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    public func configure(with presentableItem: StoreViewPresentable) {
        // To be override by subclasses
    }
}
