//
//  OffersWorker.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/19/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

protocol OffersStoreProtocol {
    func fetchOffers(completionHandler: @escaping (() throws -> [Offer]?) -> Void)
}

class OffersWorker {
    var offersStore: OffersStoreProtocol
    
    init(offersStore: OffersStoreProtocol) {
        self.offersStore = offersStore
    }
    
    func fetchOffers(completionHandler: @escaping ([Offer]?) -> Void) {
        offersStore.fetchOffers { (offers: () throws -> [Offer]?) -> Void in
            do {
                let offers = try offers()
                DispatchQueue.main.async {
                    completionHandler(offers)
                }
            } catch {
                DispatchQueue.main.async {
                    completionHandler([])
                }
            }
        }
    }
}
