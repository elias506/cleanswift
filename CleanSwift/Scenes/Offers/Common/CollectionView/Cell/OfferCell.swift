//
//  OfferCell.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

class OfferCell: UICollectionViewCell, CellIdentifiable, NibLoading {
    @IBOutlet weak var retailerNameLabel: UILabel!
    @IBOutlet weak var offerDateLabel: UILabel!
    @IBOutlet weak var couponLabel: UILabel!
    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var couponViewHeightConstraint: NSLayoutConstraint!
    
    private struct CouponViewVisibility {
        let visible = CGFloat(30.0)
        let hidden = CGFloat(0.0)
    }
    
    private struct CellSize {
        let coupon = CGSize(width: 180.0, height: 240.0)
        let catalog = CGSize(width: 180.0, height: 290.0)
    }
    
    public static func cellRatio(for type: OfferCellType) -> CGFloat {
        switch type {
        case .catalog:
            return CellSize().catalog.height / CellSize().catalog.width
        case .coupon:
            return CellSize().coupon.height / CellSize().coupon.width
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func configure(with presentableItem: OfferCellPresentable) {
        retailerNameLabel.text = presentableItem.storeNameValue
        offerDateLabel.text = presentableItem.offerDate
        offerImageView.image = presentableItem.offerImage
        couponLabel.text = presentableItem.couponText
        
        switch presentableItem.offerType {
        case .catalog:
            couponViewHeightConstraint.constant = CouponViewVisibility().hidden
        case .coupon:
            couponViewHeightConstraint.constant = CouponViewVisibility().visible
        }
    }
}
