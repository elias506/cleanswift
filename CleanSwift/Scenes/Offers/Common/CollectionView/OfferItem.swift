//
//  OfferItem.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/21/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import Foundation

struct OfferItem {
    var sectionName: String
    var presentableItems: [OfferCellPresentable]
    
    public func presentableItem(at row: Int) -> OfferCellPresentable? {
        return presentableItems[row]
    }
}
