//
//  HeaderView.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

class HeaderView: UICollectionReusableView, NibLoading, CellIdentifiable {
    public static let height = CGFloat(50.0)
    @IBOutlet weak var titleLabel: UILabel!
    
    public func configure(with presentableItem: HeaderViewPresentable) {
        titleLabel.text = presentableItem.title
    }
}
