//
//  HeaderViewAppearance.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

struct HeaderViewAppearance: HeaderViewPresentable {
    let titleText: String
    
    var title: String {
        return titleText
    }
}
