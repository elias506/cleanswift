//
//  OffersDataSource.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/20/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

protocol OffersDataSourceDelegate: class {
    func didTapOnCell(at indexPath: IndexPath)
}

class OffersDataSource: NSObject {
    let collectionView: UICollectionView
    var offers = [OfferItem]()
    weak var delegate: OffersDataSourceDelegate?
    
    init(collectionView: UICollectionView, delegate: OffersDataSourceDelegate, offers: [OfferItem]) {
        self.collectionView = collectionView
        
        self.offers = offers
        
        collectionView.register(OfferCell.loadNib(), forCellWithReuseIdentifier: OfferCell.cellIdentifier)
        collectionView.register(HeaderView.loadNib(), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: HeaderView.cellIdentifier)
        super.init()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.delegate = delegate
    }
    
    public func reloadData() {
        self.collectionView.reloadData()
    }
}

extension OffersDataSource: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didTapOnCell(at: indexPath)
    }
}
extension OffersDataSource: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return offers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers[section].presentableItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OfferCell.cellIdentifier, for: indexPath) as? OfferCell else {
            return UICollectionViewCell()
        }
        
        if let presentableItem = offers[indexPath.section].presentableItem(at: indexPath.row) {
            cell.configure(with: presentableItem)
        }
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.collectionView.frame.size.width, height: HeaderView.height)
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = headerViewForSection(indexPath: indexPath) else {
            return UICollectionReusableView()
        }
        
        return header
    }
    
    func headerViewForSection(indexPath: IndexPath) -> UICollectionReusableView? {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: HeaderView.cellIdentifier, for: indexPath) as? HeaderView else {
            return nil
        }
        let appearance = HeaderViewAppearance(titleText: offers[indexPath.section].sectionName.uppercased())
        header.configure(with: appearance)
        
        return header
    }
}

extension OffersDataSource: UICollectionViewDelegateFlowLayout {
    struct Constants {
        static let topSpace = CGFloat(8.0)
        static let sideSpace = CGFloat(7.0)
        static let itemsPerPage = UIDevice.current.userInterfaceIdiom == .pad ? 3 : 2
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellRatio = CGFloat(0.0)
        switch indexPath.section {
        case 0:
            cellRatio = OfferCell.cellRatio(for: .catalog)
        case 1:
            cellRatio = OfferCell.cellRatio(for: .coupon)
        default:
            break
        }
        
        let itemSizer = ItemSizer(view: collectionView, spacing: Constants.sideSpace, ratio: cellRatio, itemsPerPage: Constants.itemsPerPage)
        
        return CGSize(width: itemSizer.width, height: itemSizer.height)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.sideSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.topSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: Constants.topSpace, left: Constants.sideSpace, bottom: Constants.topSpace, right: Constants.sideSpace)
    }
}
