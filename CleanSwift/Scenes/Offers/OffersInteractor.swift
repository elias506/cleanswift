//
//  OffersInteractor.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright (c) 2018 Elias Esquivel. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol OffersBusinessLogic {
    func fetchOffers(request: Offers.FetchOffers.Request)
}

protocol OffersDataStore {
    var offerItems: [OfferItem]? { get }
}

class OffersInteractor: OffersBusinessLogic, OffersDataStore {
    var offerItems: [OfferItem]?
    var presenter: OffersPresentationLogic?
    var offersWorker = OffersWorker(offersStore: OffersAPI())
    
    // MARK: - Fetch Offers -
    func fetchOffers(request: Offers.FetchOffers.Request) {
        offersWorker.fetchOffers { (offers) in
            let coupons = offers?.filter{ $0.coupon != nil }
            let catalogs = offers?.filter{ $0.coupon == nil }
            var offerItems = [OfferItem]()
            
            if let catalogs = catalogs {
                let catalogItems = OfferItem(sectionName: "Catálogos", presentableItems: catalogs)
                offerItems.append(catalogItems)
            }
            
            if let coupons = coupons {
                let couponItems = OfferItem(sectionName: "Cupones", presentableItems: coupons)
                offerItems.append(couponItems)
            }
            
            self.offerItems = offerItems
            let response = Offers.FetchOffers.Response(offers: offerItems)
            self.presenter?.presentOffers(response: response)
        }
    }
}
