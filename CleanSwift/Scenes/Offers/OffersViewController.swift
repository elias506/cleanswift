//
//  OffersViewController.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright (c) 2018 Elias Esquivel. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol OffersDisplayLogic: class {
    func displayOffers(viewModel: Offers.FetchOffers.ViewModel)
}

class OffersViewController: UIViewController, OffersDisplayLogic {
    var interactor: OffersBusinessLogic?
    var router: (NSObjectProtocol & OffersRoutingLogic & OffersDataPassing)?
    var selectedIndexPath: IndexPath?
    
    public lazy var dataSource: OffersDataSource = OffersDataSource(collectionView: self.collectionView, delegate: self, offers: [OfferItem]())
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - View lifecycle -
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }
    
    // MARK: - Object lifecycle -
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - Setup -
    private func setup() {
        let viewController = self
        let interactor = OffersInteractor()
        let presenter = OffersPresenter()
        let router = OffersRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: - Routing - 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    func loadData() {
        let request = Offers.FetchOffers.Request()
        interactor?.fetchOffers(request: request)
    }
    
    func displayOffers(viewModel: Offers.FetchOffers.ViewModel) {
        dataSource.offers = viewModel.offers
        dataSource.reloadData()
    }
}

extension OffersViewController: OffersDataSourceDelegate {
    func didTapOnCell(at indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
        self.performSegue(withIdentifier: "ShowStore", sender: self)
    }
}

