//
//  Coupon.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import ObjectMapper

struct Coupon: Mappable {
    var buttons: [Button]?
    var description: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        buttons      <- map["Buttons"]
        description  <- map["description"]
    }
}
