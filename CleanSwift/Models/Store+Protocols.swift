//
//  Store+Protocols.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/23/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import Foundation

extension Store: StoreViewPresentable {
    var storeNameValue: String? {
        guard let name = self.name, !name.isEmpty else {
            return nil
        }
        return name
    }
    
    var viewState: StoreViewState {
        return .hasNearStore
    }
    
    var phoneNumberValue: String? {
        guard let phoneNumber = self.phoneNumber, !phoneNumber.isEmpty else {
            return nil
        }
        return "Teléfono: " + phoneNumber
    }
    
    var addressValue: String? {
        guard let address = self.address, !address.isEmpty else {
            return nil
        }
        
        var result = "Dirección: " + address
        
        if let city = self.city, !city.isEmpty {
            result.append(", " + city)
        }
        
        return result
    }
    
    var webSiteUrl: String? {
        guard let url = self.url, !url.isEmpty else {
            return nil
        }
        return "Sitio web: " + url
    }
}
