//
//  Store.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import ObjectMapper

struct Store: Mappable {
    var id: String?
    var name: String?
    var phoneNumber: String?
    var address: String?
    var city: String?
    var zipCode: String?
    var url: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        id          <- map["store_id"]
        name        <- map["store_name"]
        phoneNumber <- map["phone_number"]
        address     <- map["store_address"]
        city        <- map["store_city"]
        zipCode     <- map["store_zip_code"]
        url         <- map["web_url"]
    }
}
