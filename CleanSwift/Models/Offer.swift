//
//  Catalog.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import ObjectMapper

struct Offer: Mappable {
    var catalogId: String?
    var coupon: Coupon?
    var endDate: String?
    var retailerId: String?
    var retailerName: String?
    var nearestStore: Store?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        catalogId    <- map["catalog_id"]
        coupon       <- map["coupon"]
        endDate      <- map["date_end"]
        retailerId   <- map["retailer_id"]
        retailerName <- map["retailer_name"]
        nearestStore <- map["nearest_store"]
    }
}
