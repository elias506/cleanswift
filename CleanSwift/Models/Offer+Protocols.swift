//
//  Offer+Protocols.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import UIKit

extension Offer: OfferCellPresentable {
    var storePresentable: StoreViewPresentable {
        if let store = nearestStore {
            return store
        }
        return StoreNotFound()
    }
    
    var storeNameValue: String? {
        return self.retailerName
    }
    
    var offerDate: String? {
        guard let endDate = self.endDate, let interval = Double(endDate) else {
            return nil
        }
        return DateHelper.convertIntervalToStringDate(interval:interval, using: "dd/MM/yyyy")
    }
    
    var offerImage: UIImage? {
        let name = isCoupon() ? "coupon" : "catalog"
        return UIImage(named: name)
    }
    
    var offerType: OfferCellType {
        return isCoupon() ? .coupon : .catalog
    }
    
    var couponText: String? {
        return self.isCoupon() ? "CUPÓN" : nil
    }
    
    private func isCoupon() -> Bool {
        return self.coupon != nil
    }
    
    struct StoreNotFound: StoreViewPresentable {
        var viewState: StoreViewState {
            return .nearStoreNotFound(message: "No hemos encontrado tiendas cercanas")
        }
        var phoneNumberValue: String?
        var addressValue: String?
        var cityValue: String?
        var webSiteUrl: String?
        var storeNameValue: String?
    }
}
