//
//  Button.swift
//  CleanSwift
//
//  Created by Elias Esquivel on 10/18/18.
//  Copyright © 2018 Elias Esquivel. All rights reserved.
//

import ObjectMapper

struct Button: Mappable {
    var action: String?
    var text: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        action <- map["action"]
        text   <- map["text"]
    }
}
